; If you have trouble reading the long comments and variable names then I highly recommend picking up the book "The Speed reading Book" By Tony buzan ISBN:  9780563487029, 056348702X
; However, I know the comments and variable naming leaves something to be desired so if you have any suggestions please contact me on discord at Jakob#6989 or on gmail at debug.bucket@gmail.com and I'll put them in if they work better
; I also welcome any ideas and suggestions on how to make the code more efficient
; Discord is particularly useful because of the inbuilt screensharing utility
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SetBatchLines -1
ListLines Off
; Standardizes the encodings of the event files
FileEncoding, CP20127

; Stops trimming spaces, tabs, ans so on
AutoTrim, off
; Starting string to search for in the text
start_country       := "country_event ="
; Secondary string to search for in the text
start_province      := "province_event ="

; Stopping string to decide where to stop searching the text
stop        := "}"

; Set index nr for the extracted event files name extension
eventIndex := 0

; A file-loop can disrupt itself if it creates or renames files or folders within its own purview. For example, if it renames files via FileMove or other means, each such file might be found twice: once as its old name and again as its new name. To work around this, rename the files only after creating a list of them. For example:

; Load files to be split into files1.txt and files2.txt etc from the folder the script is in
; First make a list of the files
FileList=
Loop, Files, *.txt
    FileList=%FileList%%A_LoopFileName%`n
; Then go throught the list one file at a time
Loop, Parse, FileList, `n
{
    ;Reset eventTextSource for every loop
    eventTextSource:=""

    ; Load the currently looped file into the eventTextSource variable for reading and further manipulation
    FileRead,eventTextSource,%A_LoopField%

    ; Reset eventIndex for each and every read file
    eventIndex:=0
    loop
    {
        ; Increase eventIndex number for every loop so that every event file will have a unique and ordered name
        eventIndex++

        ; Reset extractedEventCode so we cA_LoopReadLinean extract another event
        extractedEventCode:=""

        ; Extract the start_province events from the current eventTextSource
        extractedEventCode := loopCurrentFile_and_ExtractEventCodeFromSource(eventTextSource, start_province, stop)
        
        ; Extract the start_country events after all start_province events are extracted
        if (extractedEventCode="")
        {
            extractedEventCode:=loopCurrentFile_and_ExtractEventCodeFromSource(eventTextSource,start_country,stop)
            eventTextSource:=cleanCurrentFileFromExtractedEventCode(eventTextSource,start_country,stop)
        }
        else
        {
            ; Remove the found province event from source to complete extraction process if the province event was found.
            eventTextSource:=cleanCurrentFileFromExtractedEventCode(eventTextSource, start_province, stop)
        }

        If(extractedEventCode="")
            break
        else
        {
            sourceFilename:=A_LoopField
            ; Trim the.txt off for the indexnumber insertion
            StringTrimRight,sourceFileNameTrimmedOfTxt,sourceFilename, 4

            ; Fetch namespace from eventid
            namespace:=fetchNamespace(extractedEventCode)

            ; Delete the source file so there are no conflicts
            FileDelete,%sourceFilename%

            ; Save the namespace first and put it on the top of the event
            FileAppend,%namespace%,%A_ScriptDir%\%sourceFileNameTrimmedOfTxt%%eventIndex%.txt

            FileAppend,`n,%A_ScriptDir%\%sourceFileNameTrimmedOfTxt%%eventIndex%.txt

            ; Save the event as a seperate file with the same source filename but with a index nr at the end of the filename
            FileAppend,%extractedEventCode%,%A_ScriptDir%\%sourceFileNameTrimmedOfTxt%%eventIndex%.txt

            ; add an extra empty line to the end for good measure
            FileAppend,`n,%A_ScriptDir%\%sourceFileNameTrimmedOfTxt%%eventIndex%.txt

            ; Save the changes made when extracting the event (extractedEventCode) from the source file (eventTextSource)
            FileAppend,%eventTextSource%,%A_ScriptDir%\%sourceFilename%
            continue
        }
    }
}
Exit

; Function to copy a whole event code bracket
loopCurrentFile_and_ExtractEventCodeFromSource(eventTextSource, start, stop)
{   
	;Variable that stores Output - reset the result variable before you add new stuff to it
	foundEventCode:="" 
	;Variable to track when to start/stop recording
	record:=False
	;Variable to track how many open brackets there are
	openB:=0 

    ;Make a placeholder for eventTextSource so that the found event lines in eventTextSource can be deleted from the source during extraction
    PlaceholderEventTextSource=%eventTextSource%

    ;Remove any values that eventTextSource holds so that the new values without the found eventtext can be put in
    eventTextSource:=""
	;Loop through each line of the variable that holds the text value (contents) of the source file
	Loop,Parse,PlaceholderEventTextSource,`n
	{ 
        ; First, check for start string to start recording
		if (IfTheStartStringIsFoundThisVariableBecomesTrue:=InStr(A_LoopField,start))
            record:=true

        ; If record not found, meaning no inString match found in this line. Restart loop and Go to next line instead.
        ; Keep looping
        if (record=false)
            continue
        ; Check if a new set of brackets has been opened
        if (InStr(A_LoopField,"{")>0)
            openB++

        ; Check if a new set of brackets has been opened
        if (InStr(A_LoopField,"}")>0)
            openB-- 

        ; If the start spot has been found, write the field to your foundEventCode variable. Field refers to the lines of code in the txt starting from the line where the start spot is and ending where the loop finds the stop string & openbrackets is less than 1
        ; Don't forget to add a new line
        foundEventCode.= A_LoopField "`n"

        ; Check after appending the foundEventCode for end string being it needs to be included in the return
        if (A_LoopField=stop)&&(openB<1)
            Break
    }
    ; Return the finished product to the caller. The global eventTextSource variable has already been updated.
    return foundEventCode
}
; Function to delete a whole previously extracted/copied event code bracket
cleanCurrentFileFromExtractedEventCode(eventTextSource,start,stop)
{   
    ;Variable that stores Output - reset the result variable before you add new stuff to it
    cleanedEventTextSource:=""
    ;Variable to track when to start/stop recording
    record:=False
    ;Variable to track how many open brackets there are
    openB:=0

    ; Variable to track how many events have been extracted
    NumberOfEventsExtracted:=0

    ;Loop through each line of the variable that holds the text value (contents) of the source file
    Loop,Parse,eventTextSource,`n,`r
    { 
        ; First, check for start string to start recording
        if (IfTheStartStringIsFoundThisVariableBecomesTrue:=InStr(A_LoopField,start)) && (NumberOfEventsExtracted<1)
        {
            record:=True
            NumberOfEventsExtracted++
        }

        ; If record not found, meaning no inString match found in this line. Restart loop and Go to next line instead.
        ; Keep looping
        if (record=False)
        {        
            cleanedEventTextSource .= A_LoopField "`n"
            continue
        }

        ; Check if a new set of brackets has been opened
        if (InStr(A_LoopField,"{")>0)
            openB++

        ; Check if a new set of brackets has been opened
        if (InStr(A_LoopField,"}")>0)
            openB--

        ; Check after appending the foundEventCode for end string being it needs to be included in the return
        if (A_LoopField=stop)&&(openB<1)
        {
            record:=False
            continue
        }
        continue
    }
    ; Return the finished product to the caller. The global eventTextSource variable has already been updated.
    return cleanedEventTextSource
}
; function to use regex to extract namespaces because I like to torture myself by learning and then promptly forgetting about regex. It hurts so good.
fetchNamespace(extractedEventCode)
{
    ; Variable that stores Output
    namespaceValue:=""

    ; Find a regexmatch in the extractedEventcode so the namespace can be added
    RegExMatch(extractedEventCode, "(?:id\s=\s)(\w+)", OutputVar, StartingPosition := 1)
    namespaceValue := "namespace = " OutputVar1 "`n"

    ; Return the finished product to the caller.
    return namespaceValue
}