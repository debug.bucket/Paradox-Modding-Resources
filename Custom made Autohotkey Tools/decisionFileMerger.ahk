; If you have trouble reading the long comments and variable names then I highly recommend picking up the book "The Speed reading Book" By Tony buzan ISBN:  9780563487029, 056348702X
; However, I know the comments and variable naming leaves something to be desired so if you have any suggestions please contact me on discord at Jakob#6989 or on gmail at debug.bucket@gmail.com and I'll put them in if they work better
; I also welcome any ideas and suggestions on how to make the code more efficient
; Discord is particularly useful because of the inbuilt screensharing utility
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SetBatchLines -1
ListLines Off
; Standardizes the encodings of the event files according to the most commonly observed vanilla eu4 event encoding
FileEncoding, CP20127

; Stops trimming spaces, tabs, and so on
AutoTrim, off

; The limiters for starting and stopping recording of events are done through regex. I will put the regex variable under this comment once I figured out how that works, and when I think it is needed. 

; Set index nr for the extracted event files name extension
eventIndex := 0

Msgbox, 4, , decision merging will soon start. Please wait for the finish message.,5

; A file-loop can disrupt itself if it creates or renames files or folders within its own purview. For example, if it renames files via FileMove or other means, each such file might be found twice: once as its old name and again as its new name. To work around this, rename the files only after creating a list of them. For example:

; Load files to be split into files1.txt and files2.txt etc from the folder the script is in
; First make a list of the files
FileList=
Loop, Files, *.txt
    FileList=%FileList%%A_LoopFileName%`n

    ; Save the marker text first and put it on the top of the decision txt
    FileAppend,country_decisions = {,%A_ScriptDir%\MergedEventCode.txt

; Then go throught the list one file at a time
Loop, Parse, FileList, `n
{
    ;Reset eventTextSource for every loop
    eventTextSource:=""

    ; Load the currently looped file into the eventTextSource variable for reading and further manipulation
    FileRead,eventTextSource,%A_LoopField%



    ; Reset eventIndex for each and every read file
    eventIndex:=0
    loop
    {
        ; Reset extractedEventCode to extract another event
        extractedEventCode:=""

        ; Extract the start_province events from the current eventTextSource
        extractedEventCode := loopCurrentFile_and_ExtractEventCodeFromSource(eventTextSource)
    
        ; Remove the found province event from source to complete extraction process
        eventTextSource := cleanCurrentFileFromExtractedEventCode(eventTextSource)
 
        If(extractedEventCode="")
            break
        else
        {
            sourceFilename:=A_LoopField

            ; Fetch namespace from eventid
            namespace:=fetchNamespace(extractedEventCode)

            ; Delete the source file so there are no conflicts
            FileDelete,%sourceFilename%

            ; Make a new line just in case its needed
            FileAppend,`n,%A_ScriptDir%\MergedEventCode.txt

            ; Save the merged decisions
            FileAppend,%extractedEventCode%,%A_ScriptDir%\MergedEventCode.txt
            continue
        }
    }
    FileDelete,%A_LoopField%
}
    ; add an extra empty line to the end for good measure plus the end marker text
    FileAppend,},%A_ScriptDir%\MergedEventCode.txt

Msgbox, Event splitting is now finished.

Exit

; Function to copy a whole PROVINCE event code bracket
loopCurrentFile_and_ExtractEventCodeFromSource(eventTextSource)
{   
    ;Variable that stores Output - reset the result variable before you add new stuff to it
    foundEventCode:="" 

    ;Variable to track when to start/stop recording
    record:=False

    ;Variable to reset and store regex start and stop strings. Not needed since it resets after every return but listed here for convenience.
    FoundPosStart := 0
    FoundPosEnd := 0

    ;Loop through each line of the variable that holds the text value (contents) of the source file
    Loop, Parse, eventTextSource,`n
    { 
        FoundPosStart := RegExMatch(A_LoopField, "(?<=([[:blank:]])).*(?= = {)") 

        ; First, check for start string to start recording
        if (FoundPosStart > 0) 
            record:=True

        ; If record not found, meaning no inString match found in this line. Restart loop and Go to next line instead.
        ; Keep looping
        if (record = False)
            continue

        ; If the start spot has been found, write the field to your foundEventCode variable. Field refers to the lines of code in the txt starting from the line where the start spot is and ending where the loop finds the stop string & openbrackets is less than 1
        ; Don't forget to add a new line
        foundEventCode .= A_LoopField "`n"

        FoundPosEnd := RegExMatch(A_LoopField, "^(\t\})")

        ; Check after appending the foundEventCode for end } bracket via regex since it needs to be included in the return
        if (FoundPosEnd > 0)
            break
    }
    ; Return the finished product to the caller. The global eventTextSource variable has already been updated.
    return foundEventCode
}
; Function to delete a whole previously extracted/copied event code bracket
cleanCurrentFileFromExtractedEventCode(eventTextSource)
{   
    ; Autotrim removes whitespaces from start and end of strings. Useful some places. Useless for matching end code brackets since it removes preceeding whitesspaces and makes all } code brackets look like end brackets.
    autotrim, off
    ;Variable that stores Output - reset the result variable before you add new stuff to it
    cleanedEventTextSource:=""
    ;Variable to track when to start/stop recording
    record:=False

    ; Variable to track how many events have been extracted
    NumberOfEventsExtracted:=0

    ; Variable to track if the event to remove is currently being processed by the line recording loop
    currentlyProcessingEventToLeaveOutOfResult:=False

    ;Variable to reset and store regex start and stop strings. Not needed since it resets after every return but listed here for convenience.
    FoundPosStart := 0
    FoundPosEnd := 0

    Firstline := 0

    ;Loop through each line of the variable that holds the text value (contents) of the source file
    Loop, Parse, eventTextSource,`n
    {

        FoundPosStart := RegExMatch(A_LoopField, "(?<=([[:blank:]])).*(?= = {)") 

        ; First, check for start string to start recording
        if (FoundPosStart > 0) && (NumberOfEventsExtracted = 0)
        {
            record:=True
            currentlyExtracting:=True
            NumberOfEventsExtracted++
        }

        FoundPosEnd := RegExMatch(A_LoopField, "^(\t\})")

        ; Check after appending the foundEventCode for end } bracket via regex since it needs to be included in the return
        if (FoundPosEnd > 0) && (NumberOfEventsExtracted > 0)
        {
            record := False
            currentlyExtracting := False
        }
        ; If record not found, meaning no inString match found in this line. Restart loop and Go to next line instead.
        ; Keep looping
        if (currentlyExtracting = False) && (Firstline > 0)
        {        
            cleanedEventTextSource .= A_LoopField "`n"
            continue
        }
        Firstline++
        continue
    }
    ; Return the finished product to the caller. The global eventTextSource variable has already been updated.
    return cleanedEventTextSource
}
; function to use regex to extract namespaces because I like to torture myself by learning and then promptly forgetting about regex. It hurts so good.
fetchNamespace(extractedEventCode)
{
    ; Variable that stores Output
    namespaceValue:=""

    ; Find a regexmatch in the extractedEventcode so the namespace can be added
    RegExMatch(extractedEventCode, "(?:id\s=\s)(\w+)", OutputVar, StartingPosition := 1)
    namespaceValue := "namespace = " OutputVar1 "`n"

    ; Return the finished product to the caller.
    return namespaceValue
}